package cyProduct;

import java.util.ArrayList;

import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomContainerSet;
import org.openscience.cdk.layout.StructureDiagramGenerator;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;

import reactantpredictor.BioTransformerAPIs;
import utils.Utilities;

public class PredictionFunctions {
	
	public IAtomContainerSet makePrediction(IAtomContainer oneMole, String cyp, String outputPath, boolean useCypReact) throws Exception{
		//System.out.println(cyp);
		/**
		 * Setting up support files including feature file and model files
		 */
		if(useCypReact){
			if(!isReactant(oneMole, cyp)){
				IAtomContainerSet results = DefaultChemObjectBuilder.getInstance().newInstance(IAtomContainerSet.class);
				if(outputPath!=null){
					PredictorForAllThreeBoMs.outputResultIAtomContainerSet(results, outputPath);
				}
				return results;
			}
		}
		PredictorForAllThreeBoMs bomPred_typeOne = new PredictorForAllThreeBoMs();
		PredictorForAllThreeBoMs bomPred_typeTwo = new PredictorForAllThreeBoMs();
		PredictorForAllThreeBoMs bomPred_typeThree = new PredictorForAllThreeBoMs();
		bomPred_typeOne.setup(cyp, 1);
		bomPred_typeTwo.setup(cyp, 2);
		bomPred_typeThree.setup(cyp, 3);
		/**
		 * Make prediction using the clone of the original molecule, so all the original information and properties will be kept			
		 */
		IAtomContainer tempMole = oneMole.clone();
		AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(tempMole);
		StructureDiagramGenerator sdg = new StructureDiagramGenerator();
		sdg.setMolecule(tempMole);
		sdg.generateCoordinates();
		tempMole = sdg.getMolecule();		
		UniqueIDFunctionSet.assignUniqueID(tempMole);
		//System.out.println("Smiles of the original structure: " + sg.create(tempMole.clone()));
		/**
		 * The makePrediction function in the PredictorForAllThreeBoMs class was primarily designed for multiple molecules. The function can be modified to handle one molecule later
		 */

		//long start=System.currentTimeMillis();
		//TypeOne
		IAtomContainerSet moleculeSet = DefaultChemObjectBuilder.getInstance().newInstance(IAtomContainerSet.class);
		moleculeSet.addAtomContainer(tempMole.clone());
		ArrayList<MoleResultPair> predResultList_typeOne = bomPred_typeOne.makePrediction(moleculeSet, 1);	
		//TypeTwo
		moleculeSet = DefaultChemObjectBuilder.getInstance().newInstance(IAtomContainerSet.class);
		moleculeSet.addAtomContainer(tempMole.clone());
		ArrayList<MoleResultPair> predResultList_typeTwo = bomPred_typeTwo.makePrediction(moleculeSet, 2);	
		//TypeThree
		moleculeSet = DefaultChemObjectBuilder.getInstance().newInstance(IAtomContainerSet.class);
		moleculeSet.addAtomContainer(tempMole.clone());
		ArrayList<MoleResultPair> predResultList_Three = bomPred_typeThree.makePrediction(moleculeSet, 3);
		IAtomContainer subtrate = null;
		if(predResultList_typeOne!=null && !predResultList_typeOne.isEmpty()){
			subtrate = predResultList_typeOne.get(0).getMolecule();
		}
		ArrayList<ArrayList<IAtom>> typeOne_BoMs = PredictorForAllThreeBoMs.getTypeOneBoMList(predResultList_typeOne, subtrate);		
		if(predResultList_typeTwo!=null && !predResultList_typeTwo.isEmpty()) subtrate = predResultList_typeTwo.get(0).getMolecule();
		ArrayList<IAtom> typeTwo_BoMs = PredictorForAllThreeBoMs.getTypeTwoBoMList(predResultList_typeTwo, subtrate);
		
		if(predResultList_Three!=null && !predResultList_Three.isEmpty()) subtrate = predResultList_Three.get(0).getMolecule();
		ArrayList<IAtom> typeThree_BoMs = PredictorForAllThreeBoMs.getTypeThreeBoMList(predResultList_Three, subtrate);
		//long predict_time=System.currentTimeMillis();
		//System.out.println("Predicted TypeOne BoM");
		for(int t = 0; t < typeOne_BoMs.size(); t++){
			ArrayList<IAtom> oneBoM = typeOne_BoMs.get(t);
			int idx_one = predResultList_typeOne.get(0).getMolecule().indexOf(oneBoM.get(0));
			int idx_two = predResultList_typeOne.get(0).getMolecule().indexOf(oneBoM.get(1));
			//System.out.println(idx_one + "," + idx_two);
		}
		//System.out.println("Predicted TypeTwo BoM");
		for(int t = 0; t < typeTwo_BoMs.size(); t++){
			IAtom oneBoM = typeTwo_BoMs.get(t);
			int idx_one = predResultList_typeTwo.get(0).getMolecule().indexOf(oneBoM);
			//int idx_two = predResultList_typeOne.get(0).getMolecule().indexOf(oneBoM.get(1));
			//System.out.println(idx_one);
		}
		//System.out.println("Prediceted TypeThree BoM");
		for(int t = 0; t < typeThree_BoMs.size(); t++){
			IAtom oneBoM = typeThree_BoMs.get(t);
			int idx_one = predResultList_Three.get(0).getMolecule().indexOf(oneBoM);
			//int idx_two = predResultList_typeOne.get(0).getMolecule().indexOf(oneBoM.get(1));
			//System.out.println(idx_one);
		}
		//System.out.println("BoM prediction done");
		IAtomContainerSet results = ClarifyReactionType.arrangeReactionTypesAndPredictMetabolites(typeOne_BoMs, typeTwo_BoMs, typeThree_BoMs, moleculeSet.getAtomContainer(0));
		//long metabolite_time=System.currentTimeMillis();
		results = Utilities.removeDuplicates(results);
		//String outputPath = "C:/Users/Tian/Desktop/BioData/SOM-React/BioTransformerDB/WaitToMerge/Merged/" + "Molecule_" + i + "_metabolites.sdf";
		//String outputPath = "E:/CyProduct_results/" + "Molecule_" + i + "_metabolites.sdf";
		results = getValidMetabolites(results);
		for(int k = 0; k < results.getAtomContainerCount(); k++){
			results.getAtomContainer(k).setProperty("Enzyme", ("CYP" + cyp));
		}
		if(outputPath!=null) PredictorForAllThreeBoMs.outputResultIAtomContainerSet(results, outputPath);
		//System.out.println("Prediction time: " + (predict_time - start));
		//System.out.println("Prediction time: " + (metabolite_time - predict_time));
		return results;
	}
	/**
	 * This function will use CypReact to check if the molecule is a reactant for the input enzyme
	 * @param oneMole
	 * @param cyp
	 * @return
	 * @throws Exception
	 */
	public boolean isReactant(IAtomContainer oneMole, String cyp) throws Exception{
		//ReactantPred reactantPredictor = new ReactantPred();
		BioTransformerAPIs cypReactAPI = new BioTransformerAPIs();
		boolean isReactant = cypReactAPI.predictReactant(oneMole, cyp);
		//boolean isReactant = true;
		return isReactant;
	}
	/**
	 * This function will check every carbon atom within the molecule, see if its connection is valid, say valence.
	 * If all carbon atoms are valid, then the molecule is valid and true is returned. Otherwise it's not.
	 * @param oneMole
	 * @return
	 * @throws Exception
	 */
	public boolean validateMetabolites(IAtomContainer oneMole) throws Exception{
		for(int i = 0; i < oneMole.getAtomCount(); i++){
			IAtom oneAtom = oneMole.getAtom(i);
			if(oneAtom.getSymbol().equalsIgnoreCase("C") && (oneAtom.getImplicitHydrogenCount() + oneMole.getBondOrderSum(oneAtom)) > 4){
				return false;
			}
		}
		return true;
	}
	
	public IAtomContainerSet getValidMetabolites(IAtomContainerSet molecules) throws Exception{
		IAtomContainerSet results = DefaultChemObjectBuilder.getInstance().newInstance(IAtomContainerSet.class);
		for(int i = 0; i < molecules.getAtomContainerCount(); i++){
			if(validateMetabolites(molecules.getAtomContainer(i))){
				results.addAtomContainer(molecules.getAtomContainer(i));
			}
		}
		return results;
	}
}
